export default [
    {
        title: 'Normalize.css',
        desc: 'css reset',
        link: 'http://necolas.github.io/normalize.css/',
    },
    {
        title: 'Bulma',
        desc: '基于 Flexbox 构建的免费、开源的 CSS 框架',
        link: 'https://bulma.zcopy.site/',
    },
    {
        title: 'Animate.css',
        desc: 'css 动画库',
        link: 'https://daneden.github.io/animate.css/',
    },
    {
        title: 'Hover.css',
        desc: 'css 悬停效果',
        link: 'http://ianlunn.github.io/Hover/',
    },
    {
        title: 'Loaders.css',
        desc: '一个 css 加载动画',
        link: 'https://connoratherton.com/loaders',
    },
    {
        title: 'loading.io',
        desc: '一个在线制作loading的网站',
        link: 'https://loading.io/',
    },
    {
        title: 'SpinKit',
        desc: '另一个 css 加载动画',
        link: 'https://tobiasahlin.com/spinkit/',
    },
    {
        title: 'Transformicons',
        desc: '另一个 css 动画按钮',
        link: 'http://www.transformicons.com/',
    },
    {
        title: '谷歌字体中文版',
        desc: '谷歌字体中文版',
        link: 'https://www.font.im/',
    },
    {
        title: 'Iconfont',
        desc: '阿里矢量图标管理、交流平台',
        link: 'https://www.iconfont.cn/',
    },
    {
        title: 'Font Awesome',
        desc: '一个图标库',
        link: 'http://www.fontawesome.com.cn/',
    },
    {
        title: 'Sass',
        desc: 'css 预处理',
        link: 'https://www.sasscss.com/',
    },
    {
        title: 'PostCSS',
        desc: '是一个用 JavaScript 工具和插件转换 CSS 代码的工具',
        link: 'https://www.postcss.com.cn/',
    },
];
