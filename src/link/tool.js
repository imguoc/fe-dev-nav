export default [
    {
        title: 'CodePen',
        desc: 'JS 在线编辑器',
        link: 'https://codepen.io/',
    },
    {
        title: 'JSFiddle',
        desc: 'JS 在线编辑器',
        link: 'https://jsfiddle.net/',
    },
    {
        title: '百度脑图',
        desc: '百度脑图',
        link: 'https://naotu.baidu.com/',
    },
    {
        title: 'GitMind',
        desc: '脑图',
        link: 'https://app.gitmind.cn/',
    },
    {
        title: '菜鸟工具',
        desc: '菜鸟工具',
        link: 'https://c.runoob.com/',
    },
    {
        title: 'FANCY-BORDER-RADIUS',
        desc: '圆形可视化工具',
        link: 'https://9elements.github.io/fancy-border-radius/',
    },
    {
        title: '前端文档工具',
        desc: '前端文档工具',
        link: 'https://www.html.cn/nav/',
    },
    {
        title: '开源中国-在线工具',
        desc: '开源中国-在线工具',
        link: 'http://tool.oschina.net/',
    },
    {
        title: '大前端-前端网址导航',
        desc: '大前端-前端网址导航',
        link: 'http://www.daqianduan.com/nav/',
    },
    {
        title: 'Web前端导航',
        desc: 'Web前端导航',
        link: 'http://www.alloyteam.com/nav/',
    },
    {
        title: '小呆导航',
        desc: '小呆导航',
        link: 'https://webjike.com/web.html',
    },
    {
        title: '在线工具',
        desc: '在线工具-程序员的工具箱',
        link: 'https://tool.lu/',
    },
    {
        title: 'Jenkins',
        desc: '持续集成',
        link: 'https://jenkins.io/zh/',
    },
];
