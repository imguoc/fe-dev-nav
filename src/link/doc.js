export default [
    {
        title: 'MDN',
        desc: 'MDN Web 文档',
        link: 'https://developer.mozilla.org/zh-CN/',
    },
    {
        title: '印记中文',
        desc: '各种优质中文文档',
        link: 'https://docschina.org/',
    },
    {
        title: 'npm',
        desc: '包管理器',
        link: 'https://www.npmjs.com/',
    },
    {
        title: 'TypeScript',
        desc: 'JS 预处理',
        link: 'https://typescript.bootcss.com/',
    },
    {
        title: 'Lodash',
        desc: 'JS 工具库',
        link: 'https://www.lodashjs.com/',
    },
    {
        title: 'Regulex',
        desc: 'JS 正则可视化工具',
        link: 'https://jex.im/regulex/',
    },
    {
        title: 'ESLint',
        desc: 'JS 检查工具',
        link: 'http://eslint.cn/',
    },
    {
        title: '现代 JavaScript 教程',
        desc: '现代 JavaScript 教程',
        link: 'https://zh.javascript.info/',
    },
    {
        title: 'JavaScript 教程',
        desc: '阮一峰 JS 教程',
        link: 'https://wangdoc.com/javascript',
    },
    {
        title: 'ES6 入门教程',
        desc: '阮一峰 ECMAScript 6 入门教程',
        link: 'https://es6.ruanyifeng.com/',
    },
    {
        title: 'Promise迷你书',
        desc: 'JavaScript Promise迷你书（中文版）',
        link: 'http://liubin.org/promises-book/',
    },
    {
        title: 'Js Tips',
        desc: '日常JavaScript技巧',
        link: 'https://www.jstips.co/zh_CN/',
    },
];
