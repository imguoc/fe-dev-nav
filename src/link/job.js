export default [
    {
        name: '面试题相关',
        data: [
            {
                title: 'FE-Interview',
                desc: '目前比较全面的前端面试题',
                link: 'http://blog.poetries.top/FE-Interview-Questions/',
            },
            {
                title: 'Front-End-Interview-Notebook',
                desc: '前端面试复习笔记',
                link:
                    'https://github.com/CavsZhouyou/Front-End-Interview-Notebook',
            },
            {
                title: '木易杨前端进阶',
                desc: '木易杨前端进阶',
                link: 'https://muyiy.cn/',
            },
            {
                title: 'qianguyihao/Web',
                desc: '前端入门到进阶图文教程，超详细的Web前端学习笔记。',
                link: 'https://github.com/qianguyihao/Web',
            },
            {
                title: '前端乐园',
                desc: '资源汇总',
                link: 'https://share.aoping.club/',
            },
            {
                title: '前端进阶之道',
                desc: '前端进阶之道',
                link: 'https://yuchengkai.cn/docs/frontend/',
            },
            {
                title: '力扣(LeetCode)',
                desc: '很全的技术题',
                link: 'https://leetcode-cn.com/',
            },
            {
                title: '牛客网',
                desc: '各种面试题',
                link: 'https://www.nowcoder.com/',
            },
        ],
    },
    {
        name: '招聘网站',
        data: [
            {
                title: 'BOSS直聘',
                desc: 'BOSS直聘',
                link: 'https://www.zhipin.com/',
            },
            {
                title: '猎聘',
                desc: '猎聘',
                link: 'https://www.liepin.com/',
            },
            {
                title: '拉钩',
                desc: '拉钩',
                link: 'https://www.lagou.com/',
            },
            {
                title: '前程无忧',
                desc: '前程无忧',
                link: 'https://www.51job.com/',
            },
            {
                title: '智联招聘',
                desc: '智联招聘',
                link: 'https://www.zhaopin.com/',
            },
        ],
    },
];
