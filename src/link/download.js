export default [
    {
        name: '编辑器 - VS Code',
        data: [
            {
                title: 'VS Code下载',
                desc: '免费、开源、运行在任何环境',
                link: 'https://code.visualstudio.com/',
            },
            {
                title: 'VS Code中文文档',
                desc: 'Microsoft Visual Studio Code 中文手册',
                link: 'https://jeasonstudio.gitbooks.io/vscode-cn-doc/content/',
            },
            {
                title: 'VS Code 的中文语言包',
                desc: '此中文（简体）语言包为 VS Code 提供本地化界面',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=MS-CEINTL.vscode-language-pack-zh-hans',
            },
            {
                title: 'Settings Sync',
                desc: '同步设置及插件',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync',
            },
            {
                title: 'GitLens',
                desc: '对内置Git功能的扩展',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens',
            },
            {
                title: 'Open in Browser',
                desc: '在浏览器中打开',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=techer.open-in-browser',
            },
            {
                title: 'EditorConfig for VS Code',
                desc: '.edorconfig',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig',
            },
            {
                title: 'Path Intellisense',
                desc: '自动检索文件名',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense',
            },
            {
                title: '翻译(translate to chinese)',
                desc: '翻译插件，使用百度翻译API把其他语言翻译成中文',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=dushaobindoudou.translation',
            },
            {
                title: 'Bracket Pair Colorizer',
                desc: '彩色的括号',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer',
            },
            {
                title: 'Power Mode',
                desc: '特效',
                link:
                    'https://marketplace.visualstudio.com/items?itemName=hoovercj.vscode-power-mode',
            },
        ],
    },
    {
        name: 'Node',
        data: [
            {
                title: 'NVM-Windows',
                desc: '用于Windows的node.js版本管理',
                link: 'https://github.com/coreybutler/nvm-windows/releases',
            },
            {
                title: 'Node',
                desc: 'node下载',
                link: 'http://nodejs.cn/download/',
            },
        ],
    },
    {
        name: '终端',
        data: [
            {
                title: 'ConEmu',
                desc:
                    '方便、全面、快速和可靠的终端窗口，可以在其中托管任何终端',
                link: 'https://conemu.github.io/',
            },
            {
                title: 'Cmder',
                desc: '适用于Windows的便携式控制台模拟器',
                link: 'https://cmder.net/',
            },
        ],
    },
    {
        name: 'FTP',
        data: [
            {
                title: 'FileZilla',
                desc: 'FileZilla 免费开源的FTP解决方案',
                link: 'https://www.filezilla.cn/',
            },
            {
                title: 'Cyberduck',
                desc: 'Cyberduck',
                link: 'https://cyberduck.io/',
            },
        ],
    },
    {
        name: '浏览器',
        data: [
            {
                title: 'Chrome 开发版',
                desc: '开发者专用的 Google Chrome',
                link: 'https://www.google.cn/chrome/dev/',
            },
            {
                title: 'Chrome',
                desc: 'Chrome 离线下载',
                link:
                    'https://www.google.cn/chrome?standalone=1&platform=win64',
            },
            {
                title: 'Chrome Downloads',
                desc: '包含多个平台及版本，常用插件',
                link: 'https://www.chromedownloads.net/',
            },
            {
                title: '极简插件',
                desc: 'Chrome插件',
                link: 'https://chrome.zzzmh.cn/',
            },
            {
                title: 'Tampermonkey',
                desc: '油猴-浏览器扩展',
                link: 'http://www.tampermonkey.net/',
            },
        ],
    },
    {
        name: 'PHP',
        data: [
            {
                title: 'PhpStudy',
                desc: '为服务器环境提供最优配置的解决方案',
                link: '',
            },
            {
                title: 'Composer',
                desc: '管理PHP依赖',
                link: 'https://www.phpcomposer.com/',
            },
            {
                title: 'Adminer',
                desc: '一个PHP文件的数据库管理工具',
                link: 'https://www.adminer.org/',
            },
        ],
    },
    {
        name: '其他',
        data: [
            {
                title: 'IDM',
                desc: '下载管理器',
                link: 'http://www.internetdownloadmanager.com/',
            },
            {
                title: 'Motrix',
                desc: '免费开源，号称全能下载工具',
                link: 'https://www.motrix.app/zh-CN/',
            },
            {
                title: 'SwitchHosts',
                desc: '快速切换hosts',
                link:
                    'https://github.com/oldj/SwitchHosts/blob/master/README_cn.md',
            },
            {
                title: 'AdGuard',
                desc: '广告拦截',
                link: 'https://adguard.com/',
            },
            {
                title: 'NatApp',
                desc: '内网穿透',
                link: 'https://natapp.cn/',
            },
            {
                title: 'Adobe软件直接下载',
                desc: '不用下载Creative Cloud',
                link:
                    'https://helpx.adobe.com/cn/download-install/kb/creative-cloud-apps-download.html',
            },
            {
                title: 'MSDN, 我告诉你',
                desc: '微软下载中心',
                link: 'https://msdn.itellyou.cn/',
            },
            {
                title: '微PE',
                desc: '微PE工具箱',
                link: 'http://www.wepe.com.cn/',
            },
            {
                title: '7-Zip',
                desc: '7-Zip 是一款拥有极高压缩比的开源压缩软件',
                link: 'https://sparanoid.com/lab/7z/',
            },
            {
                title: 'Bandisoft',
                desc: 'Bandizip for Windows、Bandizip for macOS',
                link: 'https://www.bandisoft.com/',
            },
        ],
    },
];
