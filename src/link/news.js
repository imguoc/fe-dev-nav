export default [
    {
        title: 'FreeCodeCamp',
        desc: 'FCC中文版',
        link: 'https://www.freecodecamp.one/',
    },
    {
        title: '掘金',
        desc: '',
        link: 'https://juejin.im/',
    },
    {
        title: 'Awesomes',
        desc: '前端开发资源库',
        link: 'https://www.awesomes.cn/',
    },
    {
        title: '白色橡树',
        desc: '前端及移动端优秀blog',
        link: 'https://www.cnblogs.com/PeunZhang/',
    },
    {
        title: 'ljianshu/Blog',
        desc: '关注基础知识，打造优质前端博客，公众号[前端工匠]的作者',
        link: 'https://github.com/ljianshu/Blog',
    },
    {
        title: 'InfoQ',
        desc: 'infoQ前端频道',
        link: 'https://www.infoq.cn/topic/Front-end',
    },
    {
        title: '慕课网手记',
        desc: '手记、文章',
        link: 'http://www.imooc.com/article/fe',
    },
];
