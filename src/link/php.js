export default [
    {
        title: 'PhpStudy',
        desc: 'PHP集成环境',
        link: 'https://www.xp.cn/',
    },
    {
        title: 'php中文网',
        desc: '教程、手册',
        link: 'https://www.php.cn/',
    },
    {
        title: 'Composer中文网',
        desc: 'Packagist / Composer 中国全量镜像',
        link: 'https://pkg.phpcomposer.com/',
    },
    {
        title: 'Adminer',
        desc: '一个文件的数据库管理工具',
        link: 'https://www.adminer.org/',
    },
];
