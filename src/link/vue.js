export default [
    {
        name: 'Vue2',
        data: [
            {
                title: 'Vue',
                desc: '中文文档',
                link: 'https://vue.docschina.org/',
            },
            {
                title: 'Vue Loader',
                desc: 'webpack 加载单文件 *.vue 文件的 loader',
                link: 'https://vue-loader.vuejs.org/zh/',
            },
            {
                title: 'Vue CLI',
                desc: 'Vue 官方脚手架',
                link: 'https://cli.vuejs.org/zh/',
            },
            {
                title: 'Vue Docs 中文文档翻译合集',
                desc: '官方及相关库中文翻译',
                link: 'https://github.com/vuejs/vue-docs-zh-cn',
            },
            {
                title: 'Eslint Plugin Vue',
                desc: 'Vue 官方ESLint插件',
                link: 'https://eslint.vuejs.org/',
            },
            {
                title: 'Vue Devtools',
                desc: 'Vue 开发调试工具',
                link: 'https://github.com/vuejs/vue-devtools/',
            },
            {
                title: 'Vue Router',
                desc: 'Vue 路由器',
                link: 'https://router.vuejs.org/zh/',
            },
            {
                title: 'Vue Vuex',
                desc: 'Vue 状态管理器',
                link: 'https://vuex.vuejs.org/zh/',
            },
            {
                title: 'Axios',
                desc: '基于Promise的HTTP客户端，用于浏览器和node.js',
                link: 'http://www.axios-js.com/',
            },
            {
                title: 'Element UI',
                desc: '饿了么 Vue 框架',
                link: 'https://element.eleme.cn/',
            },
            {
                title: 'uni-app',
                desc: '跨端的移动端 Vue 框架',
                link: 'https://uniapp.dcloud.io/',
            },
            {
                title: 'vue2-animate',
                desc: 'Vue.js2.0动画。用于Vue的内置过渡',
                link: 'https://the-allstars.com/vue2-animate/',
            },
            {
                title: 'awesome-github-vue',
                desc: 'Vue 资源汇总',
                link: 'https://github.com/opendigg/awesome-github-vue',
            },
            {
                title: 'Vue.js 技术揭秘',
                desc: 'Vue.js 技术揭秘',
                link: 'https://ustbhuangyi.github.io/vue-analysis/',
            },
            {
                title: 'Vue源码系列-Vue中文社区',
                desc: 'Vue源码系列-Vue中文社区',
                link: 'https://vue-js.com/learn-vue/',
            },
            {
                title: 'Vue中文社区',
                desc: 'Vue中文社区',
                link: 'https://vue-js.com/',
            },
            {
                title: 'vxe-table',
                desc: 'vue 表格解决方案',
                link: 'https://github.com/xuliangzhan/vxe-table',
            },
            {
                title: 'vue-ueditor-wrap',
                desc: 'Vue + UEditor + v-model 双向绑定',
                link: 'https://github.com/HaoChuan9421/vue-ueditor-wrap',
            },
            {
                title: 'vue-i18n',
                desc: 'vue 国际化插件',
                link: 'http://kazupon.github.io/vue-i18n/zh/',
            },
        ],
    },
    {
        name: 'Vue3',
        data: [
            {
                title: 'v3文档',
                desc: 'v3中文文档',
                link: 'https://v3.cn.vuejs.org/',
            },
        ],
    },
];
