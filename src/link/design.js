export default [
    {
        name: '在线工具',
        data: [
            {
                title: 'removebg',
                desc: '抠图神器，证件照秒换背景',
                link: 'https://www.remove.bg/',
            },
            {
                title: 'Color Scheme Designer',
                desc: '一个好用配色器',
                link: 'http://www.peise.net/tools/web/',
            },
            {
                title: '美图秀秀网页版',
                desc: '图片美化工具',
                link: 'http://xiuxiu.web.meitu.com/',
            },
            {
                title: '在线ps',
                desc: '在线ps软件',
                link: 'https://www.uupoop.com/',
            },
            {
                title: '改图宝',
                desc: '快速、简单的在线修改图片工具',
                link: 'https://www.gaitubao.com/',
            },
        ],
    },
    {
        name: '其他',
        data: [
            {
                title: 'Imagine',
                desc: '看图软件',
                link: 'http://www.nyam.pe.kr/dev/imagine/',
            },
            {
                title: '智图',
                desc: '图片压缩',
                link: 'https://zhitu.isux.us/',
            },
            {
                title: '图怪兽',
                desc: '各种海报模板',
                link: 'https://818ps.com/',
            },
            {
                title: '墨刀',
                desc: '在线产品原型设计',
                link: 'https://modao.cc/',
            },
            {
                title: 'ProcessOn',
                desc: '在线思维导图',
                link: 'https://www.processon.com/',
            },
            {
                title: 'Creative Cloud 应用程序',
                desc: 'Adobe 软件直接下载',
                link:
                    'https://helpx.adobe.com/cn/download-install/kb/creative-cloud-apps-download.html',
            },
            {
                title: '设计导航',
                desc: '关于设计资源网站',
                link: 'http://hao.shejidaren.com/',
            },
        ],
    },
];
