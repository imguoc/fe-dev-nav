export default [
    {
        title: 'normalize.css',
        desc: 'CSS reset的一种方案',
        link: 'https://jerryzou.com/posts/aboutNormalizeCss/',
    },
    {
        title: 'flex.css',
        desc: 'flex布局方案',
        link: 'https://github.com/lzxb/flex.css/blob/master/docs/zh-ch.md',
    },
    {
        title: 'animate.css',
        desc: '常用的css动画库',
        link: 'https://daneden.github.io/animate.css/',
    },
    {
        title: 'vue2-animate',
        desc: 'vue过渡动画库',
        link: 'https://github.com/asika32764/vue2-animate',
    },
    {
        title: 'axios',
        desc: 'node端和浏览器端的http库',
        link: 'http://www.axios-js.com/',
    },
    {
        title: 'flyio',
        desc: '支持所有JavaScript运行环境的http库',
        link: 'https://wendux.github.io/dist/#/doc/flyio/readme',
    },
    {
        title: 'serialize-javascript',
        desc: 'js序列化。包含正则、日期、函数',
        link: 'https://github.com/yahoo/serialize-javascript',
    },
    {
        title: 'lodash',
        desc: 'js实用工具库',
        link: 'https://www.lodashjs.com/',
    },
    {
        title: 'moment',
        desc: '时间格式化',
        link: 'http://momentjs.cn/',
    },
    {
        title: 'dayjs',
        desc: 'moment轻量化方案',
        link: 'https://day.js.org/zh-CN/',
    },
    {
        title: 'big.js',
        desc: '解决js精度计算问题',
        link: 'https://github.com/MikeMcl/big.js',
    },
    {
        title: 'js-cookie',
        desc: '处理浏览器cookie',
        link: 'https://github.com/js-cookie/js-cookie',
    },
    {
        title: 'store',
        desc: '本地储存方案，默认localStrong',
        link: 'https://github.com/marcuswestin/store.js',
    },
    {
        title: 'vuex-persistedstate',
        desc: '持久化vuex状态',
        link: 'https://github.com/robinvdvleuten/vuex-persistedstate',
    },
    {
        title: 'url-parse',
        desc: '解析url',
        link: 'https://github.com/unshiftio/url-parse',
    },
    {
        title: 'ua-parser-js',
        desc: '解析userAgent',
        link: 'https://github.com/unshiftio/url-parse',
    },
    {
        title: 'kind-of',
        desc: '检测js类型',
        link: 'https://github.com/jonschlinkert/kind-of',
    },
    {
        title: 'js-base64',
        desc: 'js-base64',
        link: 'https://github.com/dankogai/js-base64',
    },
    {
        title: 'clipboard',
        desc: '现代化的拷贝文字',
        link: 'http://www.clipboardjs.cn/',
    },
    {
        title: 'postcss-px-to-viewport',
        desc: '移动端适配',
        link:
            'https://github.com/evrone/postcss-px-to-viewport/blob/master/README_CN.md',
    },
    {
        title: 'async-validator',
        desc: '表单验证',
        link: 'https://github.com/yiminghe/async-validator',
    },
    {
        title: 'file-saver',
        desc: '保存文件',
        link: 'https://github.com/eligrey/FileSaver.js',
    },
    {
        title: 'sortablejs',
        desc: '拖拽排序',
        link: 'https://github.com/SortableJS/Sortable',
    },
];
