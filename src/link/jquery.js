export default [
    {
        title: 'jQuery',
        desc: 'jQuery API 中文文档',
        link: 'https://www.jquery123.com/',
    },
    {
        title: '你不需要jQuery',
        desc: '用纯JavaScript替代jQuery的技巧',
        link: 'http://www.webhek.com/post/you-do-not-need-jquery.html',
    },
    {
        title: 'You-Dont-Need-jQuery',
        desc: 'jQuery API 替代的方法',
        link:
            'https://github.com/nefe/You-Dont-Need-jQuery/blob/master/README.zh-CN.md',
    },
    {
        title: 'Bootstrap',
        desc: '框架',
        link: 'https://www.bootcss.com/',
    },
    {
        title: 'Art-Template',
        desc: '模板引擎',
        link: 'https://aui.github.io/art-template/zh-cn/',
    },
    {
        title: 'jquery-date-range-picker',
        desc: 'jQuery日期范围选择器',
        link: 'https://longbill.github.io/jquery-date-range-picker/',
    },
    {
        title: 'Date Range Picker',
        desc: '日期范围选择',
        link: 'http://www.daterangepicker.com',
    },
    {
        title: 'fullPage',
        desc: '全屏滚动',
        link: 'https://alvarotrigo.com/fullPage/zh/',
    },
    {
        title: 'jQuery-File-Upload',
        desc: 'jQuery文件上传',
        link: 'https://blueimp.github.io/jQuery-File-Upload/',
    },
];
