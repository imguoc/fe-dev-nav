export default [
    {
        title: 'React',
        desc: 'React 文档',
        link: 'https://react.docschina.org/',
    },
    {
        title: 'Create React App',
        desc: '构建配置的 React 应用程序',
        link: 'http://docs.breword.com/facebook-create-react-app',
    },
    {
        title: 'React Router',
        desc: 'React Router 中文文档',
        link: 'http://react-guide.github.io/react-router-cn/',
    },
];
