export default [
    {
        title: 'nrm',
        desc: 'NPM registry manager',
        link: 'https://github.com/Pana/nrm',
    },
    {
        title: 'yrm',
        desc: 'YARN registry manager',
        link: 'https://github.com/i5ting/yrm',
    },
    {
        title: 'git 使用简易指南',
        desc: 'git 简易指南',
        link: 'https://www.bootcss.com/p/git-guide/',
    },
    {
        title: 'Learning Git Branching',
        desc: 'git 实战教程',
        link: 'https://learngitbranching.js.org/',
    },
    {
        title: '图解Git',
        desc: '图解Git，加深理解',
        link: 'http://marklodato.github.io/visual-git-guide/index-zh-cn.html',
    },
    {
        title: '语义化版本',
        desc: '语义化的版本控制',
        link: 'https://semver.org/lang/zh-CN/',
    },
    {
        title: 'Git For Windows',
        desc: '针对Windows的Git侧重于提供轻量级的本地工具集',
        link: 'https://gitforwindows.org/',
    },
    {
        title: 'Sourcetree',
        desc: 'Git GUI客户端',
        link: 'https://www.sourcetreeapp.com/',
    },
    {
        title: 'TortoiseGit',
        desc: '小乌龟Git客户端',
        link: 'https://tortoisegit.org/',
    },
];
