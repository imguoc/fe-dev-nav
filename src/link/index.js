const files = require.context('/', false, /\.js$/);

const obj = {};

files.keys().forEach(item => {
    if (item === './index.js') return;
    let name = item
        .split('/')
        .pop()
        .replace(/\.\w+$/, '');
    // let upperName = name.replace(/(\w)/, v => v.toUpperCase())
    // obj[upperName] = files(item)['default']
    obj[name] = files(item)['default'];
});

export default obj;
