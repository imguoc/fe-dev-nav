export default [
    {
        name: '框架',
        data: [
            {
                title: 'Element',
                desc: '基于 Vue.js 2.0 组件库',
                link: 'https://element.eleme.cn/',
            },
            {
                title: 'Element Plus',
                desc: '基于 Vue.js 3.0 组件库',
                link: 'https://element-plus.gitee.io/',
            },
            {
                title: 'vue-element-admin',
                desc: '基于 Element-UI 的后台方案',
                link: 'https://panjiachen.github.io/vue-element-admin-site/zh/',
            },
            {
                title: 'Layui',
                desc: '经典模块化前端框架',
                link: 'https://www.layui.com/',
            },
            {
                title: 'Vant',
                desc: '基于 Vue.js 的移动端组件库',
                link: 'https://vant-contrib.gitee.io/vant/#/zh-CN/',
            },
            {
                title: 'Vant Weapp',
                desc: '基于 Vue.js 的微信小程序组件库',
                link: 'https://youzan.github.io/vant-weapp/',
            },
            {
                title: 'uni-app',
                desc: '基于 Vue.js 的跨平台框架',
                link: 'https://uniapp.dcloud.net.cn/',
            },
        ],
    },
];
