import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

let originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'default',
        },
        {
            path: '/download',
            name: 'download',
        },
        {
            path: '/package',
            name: 'package',
        },
        {
            path: '/tool',
            name: 'tool',
        },
        {
            path: '/doc',
            name: 'doc',
        },
        {
            path: '/design',
            name: 'design',
        },
        {
            path: '/css',
            name: 'css',
        },
        {
            path: '/vue',
            name: 'vue',
        },
        {
            path: '/react',
            name: 'react',
        },
        {
            path: '/jquery',
            name: 'jquery',
        },
        {
            path: '/git',
            name: 'git',
        },
        {
            path: '/resource',
            name: 'resource',
        },
        {
            path: '/news',
            name: 'news',
        },
        {
            path: '/job',
            name: 'job',
        },
        {
            path: '/comment',
            name: 'comment',
            component: () => import('../views/comment'),
        },
        // error
        {
            path: '/error',
            name: 'error',
            component: () => import('../views/page-error'),
        },
        {
            // 必须最底部
            path: '*',
            redirect: '/error',
        },
    ],
});

export default router;
