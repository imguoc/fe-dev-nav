import 'normalize.css';
import 'animate.css';
import 'element-ui/lib/theme-chalk/index.css';
import '../assets/css/common.scss';

import VueParticles from 'vue-particles';
import ElementUI from 'element-ui';

export default Vue => {
    Vue.use(VueParticles);
    Vue.use(ElementUI);
    Vue.prototype.$ELEMENT = { size: 'mini' };
};
