module.exports = {
    printWidth: 80,
    tabWidth: 4,
    useTabs: false,
    singleQuote: true,
    semi: true,
    TrailingCooma: 'all',
    bracketSpacing: true,
    jsxBracketSameLinte: true,
    arrowParens: 'avoid',
    endOfLine: 'auto',
};
