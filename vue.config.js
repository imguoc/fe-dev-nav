let webpack = require('webpack');
let GitRevisionPlugin = require('git-revision-webpack-plugin');
let isProd = process.env.NODE_ENV === 'production';

module.exports = {
    devServer: {
        port: 9999,
    },

    css: {
        extract: !isProd,
        sourceMap: !isProd,
    },

    productionSourceMap: !isProd,

    configureWebpack: config => {
        let plugins = [];
        let gitRevisionPlugin = new GitRevisionPlugin();
        plugins.push(
            gitRevisionPlugin,
            new webpack.DefinePlugin({
                'process.env.VERSION': JSON.stringify(
                    gitRevisionPlugin.version()
                ),
                'process.env.COMMITHASH': JSON.stringify(
                    gitRevisionPlugin.commithash()
                ),
                'process.env.BRANCH': JSON.stringify(
                    gitRevisionPlugin.branch()
                ),
            })
        );
        config.plugins = [...config.plugins, ...plugins];
    },

    publicPath: isProd ? '/fe-dev-nav/' : '/',
};
